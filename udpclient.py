import socket
import sys

import matplotlib
# Specify other TkAgg as backend to address constant crashes when running in IDLE
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

import numpy as np

# Time stamps of the current values
current_values_t = []
# Current values
current_values_c = []


# Time stamps of the CSS
css_values_t = []
# CSS reading
css_values_c = []


# Time each of the deployment switch has been opened
verbose = True

HOST, RPORT = "localhost", 9999
data = " ".join(sys.argv[1:])

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.settimeout(1)

def poll_current():
    sock.sendto(bytes("Get current", "utf-8"), (HOST, RPORT))
    
    try:
        received = str(sock.recv(1024), "utf-8")
        if verbose: print("Get current: ", received)
        t, c = received[1:-1].split(",")
        return float(t), float(c)
        
    except Exception as e:
        print("Get Current: ", e)
        return


def poll_css():
    sock.sendto(bytes("Get CSS", "utf-8"), (HOST, RPORT))
    
    try:
        received = str(sock.recv(1024), "utf-8")
        if verbose: print("Get CSS: ", received)
        t, c = received[1:-1].split(",")
        return float(t), float(c)
        
    except Exception as e:
        print("Get CSS: ", e)
        return


def start_deployment():
    if verbose: print("Start deployment")
        
    sock.sendto(bytes("Start deployment", "utf-8"), (HOST, RPORT))
    
    try:
        received = str(sock.recv(1024), "utf-8")
    except Exception as e:
        print("Start deployment: ", e)
   

def end_deployment():
    if verbose: print("End deployment")
        
    sock.sendto(bytes("End deployment", "utf-8"), (HOST, RPORT))
    
    try:
        received = str(sock.recv(1024), "utf-8")
    except Exception as e:
        print("Start deployment: ", e)


def poll_deployment_switches():
    sock.sendto(bytes("Get deployment switch", "utf-8"), (HOST, RPORT))
    
    try:
        received = str(sock.recv(1024), "utf-8")
        if verbose: print("Get deployment switch: ", received)
        s = received[1:-1].split(",")
        deployment_time = list(map(float, s))
        return deployment_time
    except Exception as e:
        print("Get deployment switch: ", e)
    if verbose: print("Get deployment switch")
    return


start_deployment()
poll_deployment_switches()
end_deployment()

# matplotlib setup

plt.ion()

fig, (ax1, ax2) = plt.subplots(2, 1, tight_layout=True)
ax1.set_xlim([0, 90])
ax1.set_ylim([0, 90])
ax2.set_xlim([0, 90])
ax2.set_ylim([0, 90])

line1, = ax1.plot([], [],  color='blue', linewidth=1)
deployment_markers = plt.scatter([], [], color ="forestgreen",
            linewidths = 2,
            marker ="o")

line2, = ax2.plot([], [], color='orange', linewidth=1)

ax1.set_title('Supply Current')
ax1.set_xlabel('Time [s]')
ax1.set_ylabel('Current [A]')
ax1.grid(color='black', alpha=0.7, linestyle='--', linewidth=0.1)
ax2.set_title('Coarse Sun Sensor')
ax2.set_xlabel('Time [s]')
ax2.set_ylabel('')
ax2.grid(color='black', alpha=0.7, linestyle='--', linewidth=0.1)

switch1 = ax1.axvline(x = 0, ymin = 0, ymax = 90,
               color = "forestgreen", alpha = 0.3,
               linestyle='--')
switch2 = ax1.axvline(x = 0, ymin = 0, ymax = 90,
               color = "forestgreen", alpha = 0.3,
               linestyle='--')
switch3 = ax1.axvline(x = 0, ymin = 0, ymax = 90,
               color = "forestgreen", alpha = 0.3,
               linestyle='--')
switch4 = ax1.axvline(x = 0, ymin = 0, ymax = 90,
               color = "forestgreen", alpha = 0.3,
               linestyle='--')

for i in range(90):
    t, c = poll_current()
    if t is not None and c is not None:
        current_values_t.append(t)
        current_values_c.append(c)
    
    deployment_time = poll_deployment_switches()
    
    t, c = poll_css()

    if t is not None and c is not None:
        css_values_t.append(t)
        css_values_c.append(c)

    line1.set_xdata(current_values_t)
    line1.set_ydata(current_values_c)

    if deployment_time[0] != 0:
        switch1.set_xdata([deployment_time[0], 0])
    if deployment_time[1] != 0:
        switch2.set_xdata([deployment_time[1], 0])
    if deployment_time[2] != 0:
        switch3.set_xdata([deployment_time[2], 0])
    if deployment_time[3] != 0:
        switch4.set_xdata([deployment_time[3], 0])
        
    plt.plot((0, 0), (0, 1), scaley = False)
    
    line2.set_xdata(css_values_t)
    line2.set_ydata(css_values_c)
    
    fig.canvas.draw()
    fig.canvas.flush_events()

plt.ioff()

