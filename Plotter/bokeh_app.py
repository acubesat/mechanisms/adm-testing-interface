from random import random

from bokeh.layouts import column
from bokeh.models import Button, LabelSet
from bokeh.palettes import RdYlBu3
from bokeh.plotting import figure, curdoc

from globs import time, current, plot_data_len

p = figure(
    width=1200, height=600, x_axis_label="Time [sec]", 
    y_axis_label="Current [A]",
)

p.add_layout(
    LabelSet(x="Time [sec]", y="Current [A]", text="Current Monitoring"))

# add a circle renderer with a size, color, and alpha

def update():
    global plot_data_len
    global time
    global current
    
    if len(time) < 2:
        return

    diff = len(time) - plot_data_len
    plot_data_len = len(time)
    print(diff)
    print(time[-diff-1:], current[-diff-1:])

    p.line(time[-diff-1:], current[-diff-1:])
    #new_data = qu() #qu is the newdata to be updated
    #source.stream(new_data, rollover = 60)
    #print(source.data) #if you want to see new data

curdoc().add_root(p)
curdoc().add_periodic_callback(update,10)