from django.urls import path
from . import views
 
urlpatterns = [
    path("plotter", views.plotter, name="plotter"),
    path("", views.home, name="home"),  
    path("begin_deployment", views.begin_deployment, name='begin_deployment')
    ]
