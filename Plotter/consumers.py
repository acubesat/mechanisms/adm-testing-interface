import json
import Plotter.consumers
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from Plotter.views import add_sample_point

class DataConsumer(WebsocketConsumer):
    groups = ["events"]
    
    def connect(self):
        print("Connecting")
        async_to_sync(self.channel_layer.group_add)("events", self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        print("Disconnecting :(")
        async_to_sync(self.channel_layer.group_discard)("events", self.channel_name)
        pass

    def receive(self, text_data):
        print("Received", text_data)
        print(text_data)
        try:
            text_data_json = json.loads(text_data)
            if (text_data_json["event"] == 0):
                add_sample_point(text_data_json["time"], text_data_json["current"])
            if (text_data_json["event"] == 1):
                successful_deployment(text_data_json["switch"], text_data_json["time"])

        except:
            pass
        #message = text_data_json['message']