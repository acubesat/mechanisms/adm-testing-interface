from django.shortcuts import render
from django.http import HttpResponse
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from bokeh.plotting import figure
from bokeh.embed import components, server_session, server_document
from bokeh.client import pull_session

import time

from Plotter import globs

# Create your views here.
 
# time = [1.102, 2.320]
# current = [1.2, 1.4]

def home(request):

    return render(request, 'base.html')

def plotter(request):
    session = pull_session(url = "http://localhost:5006/bokeh_app")
    #bokeh_script=autoload_server(None,url = "http://localhost:5006/bokeh_app", session_id= session.id)
    bokeh_script = server_session(model=None,
                            session_id=session.id,
                            url="http://localhost:5006/bokeh_app",
                            )

    print("starting bokeh session")
    return render(request, u'plotter.html', {u'script': bokeh_script})

def add_sample_point(t, curr):
    globs.time.append(t)
    globs.current.append(curr)
    print(f"Current in {globs.time}: {globs.current}")

def successful_deployment(switch, time):
    globs.deployment_time[switch] = time
    print(f"Successful deplpoyment of switch{switch} in {time}")

def begin_deployment(request):
    # We should reload the whole session
    session = pull_session(url = "http://localhost:5006/bokeh_app")
    bokeh_script = server_session(model=None,
                            session_id=session.id,
                            url="http://localhost:5006/bokeh_app",
                            )

    print("Deployment started")
    globs.start_time = int(round(time.time() * 1000))

    print("Started time: ", globs.start_time)
    
    layer = get_channel_layer()
    async_to_sync(layer.group_send)('events', {
        'event': 2,
        'deployment_time': 90, # TODO:deployment time can be configurable
        'configuration' : 3,
    })

    return render(request, u'plotter.html', {u'script': bokeh_script})