## Prerequisites

This was tested with the following packages/frameworks:

```
Tornado version     :  6.1
Bokeh version       :  2.3.1
Django version      :  3.2
```

Later versions of Bokeh are unfortunately conflicting with later versions of Python.

Bokeh does not officially support newer versions of Django (official support was dropped around v3.0). If you are try to run this on a fresh install an error will most likely be raised: 

```sh
 raise ImproperlyConfigured(
django.core.exceptions.ImproperlyConfigured: The app label 'bokeh.server.django' is not a valid Python identifier.
```

Go to `site-packages/django/apps/config.py` and simply comment out the following lines (line 37-40):

```sh
        if not self.label.isidentifier():
            raise ImproperlyConfigured(
                "The app label '%s' is not a valid Python identifier." % self.label
            )
```

## Usage

For running the Django server, navigate to the project folder and run `python manage.py runserver`

In order to run the Bokeh server, navigate to the `/Plotter` directory and run `bokeh serve --allow-websocket-origin localost:5006 --allow-websocket-origin 127.0.0.1:8000 bokeh_app.py`

## Events

The HTTP requests must follow the following very simple JSON schema:

### ADM Testboard -> Test Interface 

#### Current logging
```json
{
    "event": 0,
    "current": "number"
}
```

##### Parameters 

`event`:
    Must be set to `0` indicating a current logging event
    
`current`:
    The logged current (A)

#### Opened Switch
```json
{
    "event": 1,
    "switch": [0, 1, 2, 3],
    "time": "number"
}
```
##### Parameters 

`event`:
    Must be set to `1` indicating an opened switch event

`switch`:
    Indicates the switch that's opened

`time`:
    The timestamp of deployment

### Test Interface -> ADM Testboard

#### Deployment Trigger

```json
{
    "event": 2,
    "deployment_time": 90,
    "configuration" : 3,
}
```

##### Parameters 

`event`:
    Must be set to `2` indicating a deployment trigger event

`deployment_time`:
    Time the resistors will burn for

`configuration`: 
    1: Burn first set of resistors
    2: Burn second set of resistors
    3: Burn both sets consecutively
