#include <WiFi.h>
#include <esp_wpa2.h>
#include <esp_wifi.h>

#include <WiFiUdp.h>
#include <EthernetUdp.h>

#define SWITCH1 36
#define SWITCH2 39
#define SWITCH3 13
#define SWITCH4 15

#define PINFIRE 32

#define CURRENT_SENSOR 34

#define EAP_ID "**"
#define EAP_PASS "**"

const char* ssid = "eduroam";

char incomingPacket[255];
char  replyPacket[] = "ACK";

unsigned long timestamp = 0;

bool switches[]=  {0, 0, 0, 0};

bool switch_firing[] = {0, 0, 0, 0};
bool switch_opened[] = {0, 0, 0, 0};
bool switch_deployed[] = {0, 0, 0, 0};
unsigned long switch_timestamps[] = {0, 0, 0, 0};

bool fire_switch_pressed = 0;
bool fire_switch_pressed_timestamp = 0;


WiFiUDP UDP;
int udpPort = 9999;

// Expected current monitor measurement 5/47 ~ 1.06 A

const int EN1 = 4;
const int EN2 = 0;


void setup() {
  pinMode(SWITCH1, INPUT_PULLUP);
  pinMode(SWITCH2, INPUT_PULLUP);
  pinMode(SWITCH3, INPUT_PULLUP);
  pinMode(SWITCH4, INPUT_PULLUP);
  pinMode(PINFIRE, INPUT_PULLUP);
  pinMode(CURRENT_SENSOR, INPUT);
     
  pinMode(EN1, OUTPUT);
  pinMode(EN2, OUTPUT);
  digitalWrite (EN1, LOW);
  digitalWrite (EN2, LOW);
  // TODO: See which state corresponds to rising and which to falling interrupt
  attachInterrupt(digitalPinToInterrupt(SWITCH1), switch1_opened, RISING);
  attachInterrupt(digitalPinToInterrupt(SWITCH1), switch1_closed, FALLING);
  attachInterrupt(digitalPinToInterrupt(PINFIRE), switchfire_pressed, FALLING);
  attachInterrupt(digitalPinToInterrupt(PINFIRE), switchfire_unpressed, RISING);

  Serial.begin(115200);
  delay(10);
  WiFi.disconnect(true);
  WiFi.mode(WIFI_STA);
  Serial.println(WiFi.macAddress());
  esp_wifi_sta_wpa2_ent_set_identity((uint8_t *) EAP_ID, strlen(EAP_ID));
  esp_wifi_sta_wpa2_ent_set_username((uint8_t *) EAP_ID, strlen(EAP_ID));
  esp_wifi_sta_wpa2_ent_set_password((uint8_t *) EAP_PASS, strlen(EAP_PASS));
  esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT();
  esp_wifi_sta_wpa2_ent_enable(&config);
  WiFi.begin(ssid);
  while(WiFi.status() != WL_CONNECTED){
    Serial.println("Connecting ... ");
    delay(1000);
    }
  Serial.println("Connected!");
  delay(2000);
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());

  UDP.begin(udpPort);
 }

void loop() {
  
  int packetSize = UDP.parsePacket();
  if (packetSize) {
    int len = UDP.read(incomingPacket, 255);
    if (len > 0){incomingPacket[len] = 0;}

    if (strcmp(incomingPacket, "Get current")){
      // Send current report
      }
    if (strcmp(incomingPacket, "Get CSS")){
      // Send CSS reading
      }
    if (strcmp(incomingPacket, "Start deployment")){
      // Fire signal
      }
    if (strcmp(incomingPacket, "End deployment")){
      // End deployment
      }
    if (strcmp(incomingPacket, "Get deployment switch")){
      // Get deployment switch
      }
  }
}

void switchfire_pressed() {
  fire_switch_pressed_timestamp = millis();
}

void switchfire_unpressed() {
  if (fire_switch_pressed_timestamp - millis() < 20){
      digitalWrite (EN1, HIGH);
    }
}
 
void switch1_opened() {
  if (switch_deployed[0] == 1) return;
  switch_opened[0]  = 1;
  switch_timestamps[0] = millis();
}

void switch1_closed() {
  if (switch_deployed[0] == 1) return;
  switch_opened[0]  = 0; 
}

void switch2_opened() {
  if (switch_deployed[1] == 1) return;
  switch_opened[1]  = 1;
  switch_timestamps[1] = millis();
}

void switch2_closed() {
  if (switch_deployed[1] == 1) return;
  switch_opened[1]  = 0; 
}

void switch3_opened() {
  if (switch_deployed[2] == 1) return;
  switch_opened[2]  = 1;
  switch_timestamps[2] = millis();
}

void switch3_closed() {
  if (switch_deployed[2] == 1) return;
  switch_opened[2]  = 0; 
}

void switch4_opened() {
  if (switch_deployed[3] == 1) return;
  switch_opened[3]  = 1;
  switch_timestamps[3] = millis();
}

void switch4_closed() {
  if (switch_deployed[3] == 1) return;
  switch_opened[3]  = 0; 
}
