# Helper websocket server with Python - used for debugging purposes
import asyncio
import websockets
 
async def handler(websocket, path):
    msg = await websocket.recv()
    print("Received Message: ", msg)
    await websocket.send("pong")
 
start_server = websockets.serve(handler, "localhost", 8000)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
