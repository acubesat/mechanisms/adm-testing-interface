import asyncio
import websockets

async def send_message(msg):
    async with websockets.connect('ws://localhost:8000') as websocket:
        await websocket.send(msg)
        response = await websocket.recv()
        print(response)
 

for i in range(10):
    asyncio.get_event_loop().run_until_complete(send_message("ping"))

