# Test implementation - to be implemented in the ESP testboard
import socketserver
from math import sin
cur_time = 0
deployment_time = "[5, 17, 43, 23]"

class MyUDPHandler(socketserver.BaseRequestHandler):    
    def handle(self):
        global cur_time
        data = self.request[0].strip()
        socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(data)
        if data == b"Get current":
            send_data = f"({cur_time}, {cur_time})"
            socket.sendto(send_data.encode("utf-8"), self.client_address)
            cur_time = (cur_time + 1) % 90
        elif data == b"Start deployment":
            socket.sendto(b"ACK", self.client_address)
        elif data == b"End deployment":
            socket.sendto(b"ACK", self.client_address)
        elif data == b"Get deployment switch":
            socket.sendto(deployment_time.encode("utf-8"), self.client_address)
        elif data == b"Get CSS":
            send_data = f"({cur_time}, {90*sin(cur_time)})"
            socket.sendto(send_data.encode("utf-8"), self.client_address)

if __name__ == "__main__":
    cur_time = 0
    HOST, PORT = "localhost", 9999
    with socketserver.UDPServer((HOST, PORT), MyUDPHandler) as server:
        server.serve_forever()
